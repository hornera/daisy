# Daisy

Daisy is an Android garden tracking and managing application, developed as a senior project at Western Oregon University, 2018.

## Vision Statement 1.0

For avid gardeners who want to be able to keep track of their garden's information and give their plants the best care possible, Daisy is an Android application that will allow its users to store and access information about their garden, as well as get up-to-date weather and rainfall reports, all to be able to better care for their garden. For example, the user would be able to save customizable information about all their plants in Daisy for easy and quick reference, and attach user-created tags to each plant so that they may sort their garden in a way that makes sense to them, be it by location, by color, or by any other metric they choose. They will also get up-to-date and simple weather reports, including expected rainfall. Daisy will even inform them which plants will need to be watered and which ones should be fine for the day. Unlike other apps that may act as a garden journal, Daisy will work with the user to make sure they not only can keep track of their garden and its info, but help to give the plants the best care possible.

## Developed by

I am Abby Horner, currently a senior at Western Oregon University. This project is being developed as a part of my senior capstone for the 2017-2018 school year. The other part of my capstone project is a group effort which you can find [here](https://bitbucket.org/devonsmith/senior-project). I am using Android Studio and Kotlin for the development of Daisy.

## Using

Daisy is being built in Android Studio 3.0.1 with a minimum SDK of 15 and a build tools version of 27.0.2

Libraries being used:
* Glide 4.6.1
* Retrofit 2.3.0
* Moshi 1.5.0

APIs being used:
* [Weather Underground](https://www.wunderground.com/)

## Needs and Features

* Access a weather API (weather.com?)
* Display the weather for the current day
* Display the expected rainfall for the current day
* Display the weather for the upcoming week
* Display the expected rainfall for the upcoming week
* Create a custom database on a per-user basis
* Allows user to enter data they find useful to database for plants (Basic: Plant species, tags, water/soil requirements, physical descriptors?)
* Allows user to save pictures on a per-plant basis
* Allows users to specify water requirements per plants
* Uses water requirements and expected rainfall/lack thereof to generate a watering plan for the user
* Alerts a user when a freeze is coming

## Initial Modeling

### Architecture Diagram

![Architecture Diagram](/Documentation/Modeling/architecture.png)

### Use-Case Diagram

![Use-Case Diagram](/Documentation/Modeling/use-case.png)
