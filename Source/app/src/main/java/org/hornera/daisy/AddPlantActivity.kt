package org.hornera.daisy

import android.app.AlertDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import android.content.DialogInterface
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.CollectionReference


class AddPlantActivity : AppCompatActivity() {

    // VARIABLES
    private val TAGS_KEY = "tags"
    private val NAME_KEY = "plantName"
    private val mAuth = FirebaseAuth.getInstance()

    private lateinit var UID:String
    private lateinit var mCollRef: CollectionReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_plant)

        // Check if user is logged in. If not, send them to the login page
        if (mAuth.currentUser != null) UID = mAuth.currentUser!!.uid
        else startActivity(Intent(this, LoginActivity::class.java))
        mCollRef = FirebaseFirestore.getInstance().collection("users/$UID/plants")

        // Get button variables
        val submitButton = findViewById<Button>(R.id.addPlantSubmitButton)
        val cancelButton = findViewById<Button>(R.id.cancelAddButton)

        // Make button listeners
        submitButton.setOnClickListener({
            // TODO submit plant logic
            savePlant()
        })

        cancelButton.setOnClickListener({
            // Check if they're sure before we back out
            val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    // Yes button clicked
                    DialogInterface.BUTTON_POSITIVE -> {
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                    // No button clicked
                    DialogInterface.BUTTON_NEGATIVE -> {
                        // nothing needs to be done
                    }
                }
            }
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show()
        })
    }

    fun savePlant() {
        // Getting plant name from input
        var plantName = findViewById<EditText>(R.id.PlantNameEditText)
        var plantNameString = plantName.text.toString()

        if (plantNameString.isEmpty()) {
            return
        }

        // Create document to add data to
        var mDocRef = FirebaseFirestore.getInstance().document("users/$UID/plants/$plantNameString")

        // Getting tags and separating them where there are commas
        var tags = findViewById<EditText>(R.id.tagsEditText)
        var tagsString = tags.text.toString()
        var tagsArray = tagsString.split(",")

        // Create the map to enter into the database
        var newPlantInfo:MutableMap<String, Any> = HashMap()
        newPlantInfo[NAME_KEY] = plantNameString
        newPlantInfo[TAGS_KEY] = tagsArray

        // Stick that data in that database
        mDocRef.set(newPlantInfo).addOnCompleteListener({task ->
            if (task.isSuccessful) {
                Toast.makeText(this, "Congrats! Your plant, $plantNameString, was saved!", Toast.LENGTH_LONG).show()
                startActivity(Intent(this, MainActivity::class.java))
            }
            else {
                Toast.makeText(this, "Uh oh, something went wrong with your entry...", Toast.LENGTH_LONG).show()
            }
        })
    }
}
