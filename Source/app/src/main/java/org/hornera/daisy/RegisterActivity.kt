package org.hornera.daisy

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var emailFormEditText: EditText
    private lateinit var zipCodeEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var confirmPasswordEditText: EditText
    private lateinit var usernameEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mAuth = FirebaseAuth.getInstance()
        emailFormEditText = findViewById(R.id.registerEmailEditText)
        zipCodeEditText = findViewById(R.id.registerZipEditText)
        passwordEditText = findViewById(R.id.registerPasswordEditText)
        confirmPasswordEditText = findViewById(R.id.registerConfirmPasswordEditText)
        usernameEditText = findViewById(R.id.usernameEditText)


        // BUTTON LISTENERS
        val submitRegButton = findViewById<Button>(R.id.registerSubmitButton)
        val cancelRegButton = findViewById<Button>(R.id.registerCancelButton)

        // SUBMIT BUTTON LOGIC
        submitRegButton.setOnClickListener({
            // SET UP VARIABLES FROM EDIT TEXT FIELDS
            val email = emailFormEditText.text.toString()
            val password = passwordEditText.text.toString()
            val confirmPassword = confirmPasswordEditText.text.toString()
            val zipCode = zipCodeEditText.text.toString()
            val username = usernameEditText.text.toString()

            if (password != confirmPassword) {
                Toast.makeText(this, "Passwords don't match, try again", Toast.LENGTH_SHORT).show()
                resetField(passwordEditText)
                resetField(confirmPasswordEditText)
            } else if (!EMAIL_ADDRESS.matcher(email).matches()) {
                Toast.makeText(this, "Email is invalid, please try again", Toast.LENGTH_SHORT).show()
                resetField(emailFormEditText)
            } else if (!checkZip(zipCode)) {
                Toast.makeText(this, "The zip code is invalid, please try again!", Toast.LENGTH_SHORT).show()
                resetField(zipCodeEditText)
            } else if (username.isEmpty()) {
                Toast.makeText(this, "You must input a username, please try again!", Toast.LENGTH_SHORT).show()
            } else {
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener({ task ->
                    if (task.isSuccessful) {
                        // User created successfully
                        val user = mAuth.currentUser
                        updateUI(user)
                    }
                    else {
                        Toast.makeText(this, "Something went wrong, please try again!", Toast.LENGTH_SHORT).show()
                        resetAllFields()
                    }
                })
            }
        })


    // CANCEL BUTTON LOGIC
    cancelRegButton.setOnClickListener ({
        // Check if they're sure before we back out
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
            // Yes button clicked
                DialogInterface.BUTTON_POSITIVE -> {
                    startActivity(Intent(this, MainActivity::class.java))
                }
            // No button clicked
                DialogInterface.BUTTON_NEGATIVE -> {
                    // nothing needs to be done
                }
            }
        }
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show()
    })
}

private fun updateUI(user: FirebaseUser?) {
    // Getting the zip code again
    this.zipCodeEditText = findViewById(R.id.registerZipEditText)
    val zipCode = zipCodeEditText.text.toString()

    // Check it and the user
    if (checkZip(zipCode) && user != null) {
        // Get the user ID from the database
        val userID = user.uid

        // Find the document we want to edit (this user's)
        val mDocRef = FirebaseFirestore.getInstance().document("users/$userID")

        // Add the info
        val zipCodeInfo: MutableMap<String, Any> = HashMap()
        zipCodeInfo["Zip Code"] = zipCode
        mDocRef.set(zipCodeInfo).addOnCompleteListener({ task ->
            if (task.isSuccessful) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                // TODO zip code failed logic
            }
        })
    }

}

private fun checkZip(zipCode: String): Boolean {
    val regex = "^[0-9]{5}(?:-[0-9]{4})?"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(zipCode)
    return matcher.matches()
}

private fun resetAllFields() {
    passwordEditText.setText("")
    confirmPasswordEditText.setText("")
    emailFormEditText.setText("")
    zipCodeEditText.setText("")
    usernameEditText.setText("")
}

private fun resetField(field: EditText) {
    field.setText("")
}
}
