package org.hornera.daisy

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Build
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.json.JSONObject
import android.database.sqlite.SQLiteDatabase
import android.support.annotation.NonNull
import android.util.Log
import android.util.Log.DEBUG
import android.view.View
import android.widget.Button
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.*
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Document
import org.w3c.dom.Text


class MainActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()
    private var zipCode = "97304"

    // ONCREATE
    override fun onCreate(savedInstanceState: Bundle?) {
        // ------------------- THIS GOES FIRST -------------------
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // ------------------- CODE AFTER THIS -------------------

        // AUTH
        mAuth = FirebaseAuth.getInstance()

    }

    //MAKE OUR BUTTONS DO STUFF
    fun buttonNav(View: View) {
        // Get button pressed
        val buttonText = (View as Button).getText().toString()
        if (buttonText == "Add a Plant") {
            // Switch to add a plant view
            val add = Intent(this, AddPlantActivity::class.java)
            startActivity(add)
        }
        else if (buttonText.equals(R.string.ViewPlantsButton)) {
            // TODO add this activity and uncomment this code
            /*
            // Switch to view plants view
            var view = Intent(this, ViewPlantsActivity::class.java)
            startActivity(view)
            */
        }
    }

    // Check to see if the user is currently logged in
    override fun onStart() {
        super.onStart()
        val currentUser = mAuth.currentUser
        if (currentUser == null) updateUI(currentUser)
        else {
            getZip(currentUser)
            updateWeather(zipCode)
        }

    }

    // Make the user login if they aren't
    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    // Use the zip code and update the weather with an API call
    private fun updateWeather(zip:String) {
        // Build the API url
        val baseurl = "http://api.wunderground.com/api/"
        val key = BuildConfig.WUNDERGROUND_API_KEY
        val midurl = "/conditions/q/"
        val endurl= ".json"
        val requestURL = "$baseurl$key$midurl$zip$endurl"

        // Get our views
        val degreeTextView = findViewById<TextView>(R.id.temperatureTextView)
        val conditionTextView = findViewById<TextView>(R.id.conditionTextView)
        val locationTextView = findViewById<TextView>(R.id.locationTextView)
        val weatherIconImageView = findViewById<ImageView>(R.id.weatherIconImageView)

        // Use Volley to send the API request
        val queue = Volley.newRequestQueue(this)
        val request = JsonObjectRequest(Request.Method.GET, requestURL, null, Response.Listener { response ->
            if (response != null) {
                // It worked, we got the JSON. Now make a Parser and get to it
                val parser = JsonParser()
                val data = parser.parse(response.toString()) as JsonObject

                // Parsing the JSON....
                val city = data.getAsJsonObject("current_observation").getAsJsonObject("display_location").get("city").toString().replace("\"","")
                val state = data.getAsJsonObject("current_observation").getAsJsonObject("display_location").get("state").toString().replace("\"","")
                val temp_f = data.getAsJsonObject("current_observation").get("temp_f")
                val icon = data.getAsJsonObject("current_observation").get("icon").toString().replace("\"", "")
                val condition = data.getAsJsonObject("current_observation").get("weather").toString().replace("\"","")

                degreeTextView.text = "$temp_f\u2109"
                locationTextView.text = "$city, $state"
                conditionTextView.text = "$condition"

                // LOAD WEATHER ICON FROM INTERNET
                val iconbase = "https://icons.wxug.com/i/c/k/"
                val iconend = ".gif"

                val iconUrl = "$iconbase$icon$iconend"
                try {
                    Glide.with(this)
                            .load(iconUrl)
                            .into(weatherIconImageView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }, Response.ErrorListener { error ->
            // It didn't work
        })
        queue.add(request)
    }

    // Get the zip code of the current user from the database
    private fun getZip(user:FirebaseUser?) {
        if (user == null) updateUI(user)
        else {
            val uid = user.uid
            val docRef = db.document("users/$uid")
            docRef.get().addOnCompleteListener({
                if (it.isSuccessful) {
                    if (it.result != null && it.result.exists()) {
                        val document = it.result
                        zipCode = document.getString("Zip Code")
                        updateWeather(zipCode)
                    }
                }
                else {
                    // TODO no zip code logic...
                }
            })
        }
    }

}




