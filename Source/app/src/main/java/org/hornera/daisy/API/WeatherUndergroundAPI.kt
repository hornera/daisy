package org.hornera.daisy.API

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by super on 3/6/2018.
 */
interface WeatherUndergroundAPI {
    @GET("{key}/conditions/q/{lat},{long}.json")
    fun listConditions(@Path("key") key:String, @Path("lat") lat:Double, @Path("long") long:Double): Call<List<String>>
}