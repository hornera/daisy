package org.hornera.daisy

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class LoginActivity : AppCompatActivity() {

    // VARIABLES
    private var mAuth = FirebaseAuth.getInstance()

    // FUNCTIONS
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Get button listeners ready
        val submitButton = findViewById<Button>(R.id.submitLoginButton)
        val registerButton = findViewById<Button>(R.id.registerButton)

        // Get text edit fields
        var emailEditText = findViewById<EditText>(R.id.emailLoginTextEdit)
        var passwordEditText = findViewById<EditText>(R.id.passwordLoginTextEdit)

        submitButton.setOnClickListener({
            // When it's clicked, check user credentials
            var email = emailEditText.text.toString()
            var password = passwordEditText.text.toString()
            if (email.isEmpty() || password.isEmpty()) {
                // One or both of them is empty, so we won't even try to login with it
                loginFailed(emailEditText, passwordEditText)
            }
            else {
                // Both are filled in, let's use Firebase and check if it works
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, {task ->
                    if (task.isSuccessful) {
                        // Sign in was successful
                        var user:FirebaseUser = mAuth.currentUser!!
                        updateUI(user)
                    }
                    else {
                        // Sign in failed....
                        loginFailed(emailEditText, passwordEditText);
                    }
                })
            }


        })

        registerButton.setOnClickListener({
            startActivity(Intent(this, RegisterActivity::class.java))
        })

    }

    // UPDATE UI/ACTIVITY ON LOGIN
    private fun updateUI(user:FirebaseUser?) {
        if (user == null) {
            // We messed up.. try it again
            // Later on, maybe update this. If we have a user of null and still made it here, something is funky
            loginFailed(findViewById(R.id.emailLoginTextEdit), findViewById(R.id.passwordLoginTextEdit))
        }
        else {
            // The user is logged in! Let's do this! Send them back to the main activity so they can use their app
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    // IF LOGIN FAILED, CLEAR INPUT FIELDS AND TRY AGAIN
    private fun loginFailed(a:EditText, b:EditText) {
        Toast.makeText(this, "Oops! Something didn't work. Try again?", Toast.LENGTH_SHORT).show()
        a.setText("")
        b.setText("")
    }



}
